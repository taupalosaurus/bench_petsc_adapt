static char help[] = "Tests mesh adaptation with DMPlex and MMG.\n";

#include <stdio.h>
#include <stdlib.h>

#include <petsc.h>


#include "io_libmeshb.h"



typedef struct {
  PetscInt        Nr;                              /* The number of refinement passes */
  PetscInt        metOpt, sensor, metricAnalytic;  /* Different choices of metric */
  PetscReal       hmax, hmin;                      /* Max and min sizes prescribed by the metric */
  PetscErrorCode  (*sensor_fun)(PetscInt, PetscReal, const PetscReal[], PetscInt, PetscScalar[], void*);
} AppCtx;


/*
  Various pre-defined sensor functions to drive the adaptation.
*/
static PetscErrorCode sensor_0(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar u[], void *ctx)
{
  const PetscReal xref = 2.*x[0] - 1.;
  const PetscReal yref = 2.*x[1] - 1.;
  
  PetscFunctionBeginUser;
  u[0] = xref*xref + yref*yref;
  PetscFunctionReturn(0);
}

static PetscErrorCode sensor_1(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar u[], void *ctx)
{
  const PetscReal xref = 2.*x[0] - 1.;
  const PetscReal yref = 2.*x[1] - 1.;
  const PetscReal xy   = xref*yref;

  PetscFunctionBeginUser;
  u[0] = PetscSinReal(50.*xy);
  if (PetscAbsReal(xy) > 2.*PETSC_PI/50.) u[0] *= 0.01;
  PetscFunctionReturn(0);
}

static PetscErrorCode sensor_2(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar u[], void *ctx)
{
  const PetscReal xref = 2.*x[0] - 1.;
  const PetscReal yref = 2.*x[1] - 1.;

  PetscFunctionBeginUser;
  u[0] = 0.1*PetscSinReal(50*xref)+PetscAtanReal(0.1/(PetscSinReal(5*yref)-2*xref));
  PetscFunctionReturn(0);
}

static PetscErrorCode sensor_3(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar u[], void *ctx)
{
  const PetscReal xref = 2.*x[0] - 1.;
  const PetscReal yref = 2.*x[1] - 1.;

  PetscFunctionBeginUser;
  u[0] = PetscAtanReal(0.1/(PetscSinReal(5*yref)-2*xref)) + PetscAtanReal(0.5/(PetscSinReal(3*yref)-7*xref));
  PetscFunctionReturn(0);
}

static PetscErrorCode sensor_4(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar u[], void *ctx)
{
  PetscFunctionBeginUser;
  u[0] = PetscTanhReal(-100*(x[1]-0.5-0.25*PetscSinReal(2*PETSC_PI*x[0])))+PetscTanhReal(100*(x[1]-x[0]));
  PetscFunctionReturn(0);
}


static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscFunctionBegin;
  options->Nr     = 1;
  options->metOpt = 1;
  options->sensor = 0;
  options->metricAnalytic = 0;
  options->hmin   = 0.05;
  options->hmax   = 0.5;

  PetscOptionsBegin(comm, "", "Meshing Adaptation Options", "DMPLEX");
  PetscCall(PetscOptionsBoundedInt("-Nr", "Numberof refinement passes", "ex19.c", options->Nr, &options->Nr, NULL, 1));
  PetscCall(PetscOptionsBoundedInt("-met", "Different choices of metric (0: uniform, 1:analytic, 2:multiscale", "ex19.c", options->metOpt, &options->metOpt, NULL,0));
  PetscCall(PetscOptionsBoundedInt("-sensor", "Different choices of sensor for multiscale metric", "ex19.c", options->sensor, &options->sensor, NULL,0));
  PetscCall(PetscOptionsBoundedInt("-metricanalytic", "Different choices of analytic metric", "ex19.c", options->metricAnalytic, &options->metricAnalytic, NULL,0));
  PetscCall(PetscOptionsReal("-hmax", "Max size prescribed by the metric", "ex19.c", options->hmax, &options->hmax, NULL));
  PetscCall(PetscOptionsReal("-hmin", "Min size prescribed by the metric", "ex19.c", options->hmin, &options->hmin, NULL));

  switch (options->sensor) {
    case 0:
      options->sensor_fun = &sensor_0;
      break;
    case 1:
      options->sensor_fun = &sensor_1;
      break;
    case 2:
      options->sensor_fun = &sensor_2;
      break;
    case 3:
      options->sensor_fun = &sensor_3;
      break;
    case 4:
      options->sensor_fun = &sensor_4;
      break;
    default:
      options->sensor_fun = &sensor_1;
  }
  PetscOptionsEnd();

  PetscFunctionReturn(0);
}


static PetscErrorCode ComputeAnalyticMetric(DM dm, AppCtx *user, Vec *metric)
{
  DM                 cdm;
  Vec                coordinates;
  const PetscScalar *coords;
  PetscScalar       *met;
  PetscScalar        lambda;
  PetscReal          h;
  PetscInt           dim, i, j, vStart, vEnd, v;

  PetscFunctionBeginUser;
  lambda = 1./(user->hmax*user->hmax);
  PetscCall(DMPlexMetricCreate(dm, 0, metric));
  PetscCall(DMGetDimension(dm, &dim));
  PetscCall(DMGetCoordinateDM(dm, &cdm));
  PetscCall(DMGetCoordinatesLocal(dm, &coordinates));
  PetscCall(VecGetArrayRead(coordinates, &coords));
  PetscCall(VecGetArray(*metric, &met));
  PetscCall(DMPlexGetDepthStratum(dm, 0, &vStart, &vEnd));
  for (v = vStart; v < vEnd; ++v) {
    PetscScalar *vcoords;
    PetscScalar *pmet;

    PetscCall(DMPlexPointLocalRead(cdm, v, coords, &vcoords));
    if (dim == 2) {
      switch (user->metricAnalytic) {
      case 0:
        h = user->hmax - (user->hmax-user->hmin)*PetscRealPart(vcoords[0]);
        PetscCall(DMPlexPointLocalRef(dm, v, met, &pmet));
        for (i = 0; i < dim; ++i) {
          for (j = 0; j < dim; ++j) {
            if (i == j) {
              if (i == 0) pmet[i*dim+j] = 1/(h*h);
              else pmet[i*dim+j] = lambda;
            } else pmet[i*dim+j] = 0.0;
          }
        }
        break;
      case 1:
        h = user->hmax*PetscAbsReal(((PetscReal) 1.0)-PetscExpReal(-PetscAbsScalar(vcoords[0]-(PetscReal)0.5))) + user->hmin;
        PetscCall(DMPlexPointLocalRef(dm, v, met, &pmet));
        for (i = 0; i < dim; ++i) {
          for (j = 0; j < dim; ++j) {
            if (i == j) {
              if (i == 0) pmet[i*dim+j] = 1/(h*h);
              else pmet[i*dim+j] = lambda;
            } else pmet[i*dim+j] = 0.0;
          }
        }
        break;
      default:
        SETERRQ(PetscObjectComm((PetscObject) dm), PETSC_ERR_ARG_WRONG, "-metricanalytic = 0, 1 in 2D cannot be %d", user->metOpt);
      }
    }
    else if (dim == 3) {
      PetscScalar  x, y, r, t, d, ct, st, lr, lt, lz, h_r, h_t, h_z;

      switch (user->metricAnalytic) {
      case 0:
        h = user->hmin + 2*(0.1-user->hmin)*PetscAbsReal(vcoords[2]-0.5);
        PetscCall(DMPlexPointLocalRef(dm, v, met, &pmet));
        for (i = 0; i < dim; ++i) {
          for (j = 0; j < dim; ++j) {
            if (i == j) {
              if (i == 2) pmet[i*dim+j] = 1/(h*h);
              else pmet[i*dim+j] = lambda;
            } else pmet[i*dim+j] = 0.0;
          }
        }
        break;
      case 1:
        x = vcoords[0];
        y = vcoords[1];
        r = PetscSqrtReal(x*x+y*y);
        t = PetscAtan2Real(y,x);
        h_r = user->hmin + 2*(0.1-user->hmin)*PetscAbsReal(r-0.5);
        h_t = h_z = 0.1;
        lr = 1./(h_r*h_r);
        lt = 1./(h_t*h_t);
        lz = 1./(h_z*h_z);
        ct = PetscCosReal(t);
        st = PetscSinReal(t);
    
        PetscCall(DMPlexPointLocalRef(dm, v, met, &pmet));
        for (i = 0; i < dim*dim; ++i) { pmet[i] = 0.0;
        }
        pmet[0] = lr*ct*ct+lt*st*st;
        pmet[1] = pmet[3] = (lr-lt)*ct*st;
        pmet[2] = pmet[6] = 0;
        pmet[4] = lr*st*st+lt*ct*ct;
        pmet[5] = pmet[7] = 0;
        pmet[8] = lz;
        break;
      case 2:
        x = vcoords[0];
        y = vcoords[1];
        r = PetscSqrtReal(x*x+y*y);
        t = PetscAtan2Real(y,x);
        h_r = user->hmin + 2*(0.1-user->hmin)*PetscAbsReal(r-0.5);
        d = (0.6 - r) * 10;
        h_t = (d < 0) ? 0.1 : (d * (0.025) + (1 - d) * 0.1);
        h_z = 0.1;
        lr = 1./(h_r*h_r);
        lt = 1./(h_t*h_t);
        lz = 1./(h_z*h_z);
        ct = PetscCosReal(t);
        st = PetscSinReal(t);
    
        PetscCall(DMPlexPointLocalRef(dm, v, met, &pmet));
        for (i = 0; i < dim*dim; ++i) { pmet[i] = 0.0;
        }
        pmet[0] = lr*ct*ct+lt*st*st;
        pmet[1] = pmet[3] = (lr-lt)*ct*st;
        pmet[2] = pmet[6] = 0;
        pmet[4] = lr*st*st+lt*ct*ct;
        pmet[5] = pmet[7] = 0;
        pmet[8] = lz;
        break;
        break;
      default:
        SETERRQ(PetscObjectComm((PetscObject) dm), PETSC_ERR_ARG_WRONG, "-metricanalytic = 0, 1 in 2D cannot be %d", user->metOpt);
      }
    }
    else {
      SETERRQ(PetscObjectComm((PetscObject) dm), PETSC_ERR_ARG_WRONG, "dimension cannot be %d", dim);
    } 
  }
  PetscCall(VecRestoreArray(*metric, &met));
  PetscCall(VecRestoreArrayRead(coordinates, &coords));
  PetscFunctionReturn(0);
}


static PetscErrorCode ComputeMetricSensor(DM dm, AppCtx *user, Vec *metric)
{
  PetscSimplePointFunc funcs[1] = {*user->sensor_fun};
  DM             dmSensor, dmGrad, dmHess, dmDet;
  PetscFE        fe;
  Vec            f, g, H, determinant;
  PetscBool      simplex;
  PetscInt       dim;

  PetscFunctionBeginUser;
  PetscCall(DMGetDimension(dm, &dim));
  PetscCall(DMPlexIsSimplex(dm, &simplex));

  PetscCall(DMClone(dm, &dmSensor));
  PetscCall(PetscFECreateLagrange(PETSC_COMM_SELF, dim, 1, simplex, 1, -1, &fe));
  PetscCall(DMSetField(dmSensor, 0, NULL, (PetscObject) fe));
  PetscCall(PetscFEDestroy(&fe));
  PetscCall(DMCreateDS(dmSensor));
  PetscCall(DMCreateLocalVector(dmSensor, &f));
  PetscCall(DMProjectFunctionLocal(dmSensor, 0., funcs, NULL, INSERT_VALUES, f));
  PetscCall(VecViewFromOptions(f, NULL, "-sensor_view"));

  // Recover the gradient of the sensor function
  PetscCall(DMClone(dm, &dmGrad));
  PetscCall(PetscFECreateLagrange(PETSC_COMM_SELF, dim, dim, simplex, 1, -1, &fe));
  PetscCall(DMSetField(dmGrad, 0, NULL, (PetscObject) fe));
  PetscCall(PetscFEDestroy(&fe));
  PetscCall(DMCreateDS(dmGrad));
  PetscCall(DMCreateLocalVector(dmGrad, &g));
  PetscCall(DMPlexComputeGradientClementInterpolant(dmSensor, f, g));
  PetscCall(VecDestroy(&f));
  PetscCall(VecViewFromOptions(g, NULL, "-gradient_view"));

  // Recover the Hessian of the sensor function
  PetscCall(DMClone(dm, &dmHess));
  PetscCall(DMPlexMetricCreate(dmHess, 0, &H));
  PetscCall(DMPlexComputeGradientClementInterpolant(dmGrad, g, H));
  PetscCall(VecDestroy(&g));
  PetscCall(VecViewFromOptions(H, NULL, "-hessian_view"));

  // Obtain a metric by Lp normalization
  PetscCall(DMPlexMetricCreate(dm, 0, metric));
  PetscCall(DMPlexMetricDeterminantCreate(dm, 0, &determinant, &dmDet));
  PetscCall(DMPlexMetricNormalize(dmHess, H, PETSC_TRUE, PETSC_TRUE, *metric, determinant));
  PetscCall(VecDestroy(&determinant));
  PetscCall(DMDestroy(&dmDet));
  PetscCall(VecDestroy(&H));
  PetscCall(DMDestroy(&dmHess));
  PetscCall(DMDestroy(&dmGrad));
  PetscCall(DMDestroy(&dmSensor));
  PetscFunctionReturn(0);
}



static PetscErrorCode ComputeMetric(DM dm, AppCtx *user, Vec *metric)
{
  PetscReal      lambda;

  PetscFunctionBeginUser;
  if (user->metOpt == 0) {
    /* Specify a uniform, isotropic metric */
    lambda = 1./(user->hmax*user->hmax);
    PetscCall(DMPlexMetricCreateUniform(dm, 0, lambda, metric));
  } else if (user->metOpt == 2) {
    PetscCall(ComputeMetricSensor(dm, user, metric));
  } else if (user->metOpt == 1) {
    PetscCall(ComputeAnalyticMetric(dm, user, metric));
  }
  else {
    SETERRQ(PetscObjectComm((PetscObject) dm), PETSC_ERR_ARG_WRONG, "-met = 0, 1, 2 in 2D cannot be %d", user->metOpt);
  }
  PetscFunctionReturn(0);
}




int main(int argc, char* argv[])
{
  DM          dm, dma;
  Vec         metric;
  AppCtx      user;                 /* user-defined work context */
  MPI_Comm    comm;
  PetscInt    dim, r;
  const char  bdLabelName[] = "boundary_label";

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;
  PetscCall(ProcessOptions(comm, &user));

  // 1- read a mesh
  dim = 3;
  if (dim == 2) {
    PetscCall(DMPlexCreateGmfFromFile_2d("square", bdLabelName, &dm));
  }
  else if (dim == 3) {
    PetscCall(DMPlexCreateGmfFromFile_3d("data/meshes/3D/cube-cylinder", bdLabelName, &dm));
  }

  for (r = 0; r < user.Nr; ++r) {
    DMLabel label;

    // 2- build metric
    PetscCall(ComputeMetric(dm, &user, &metric));
    PetscCall(DMGetLabel(dm, bdLabelName, &label));

    // 3- call MMG
    PetscCall(DMAdaptMetric(dm, metric, label, NULL, &dma));
    PetscCall(VecDestroy(&metric));
    PetscCall(PetscObjectSetName((PetscObject) dma, "DMadapt"));
    PetscCall(PetscObjectSetOptionsPrefix((PetscObject) dma, "adapt_"));
    PetscCall(DMViewFromOptions(dma, NULL, "-dm_view"));
    PetscCall(DMDestroy(&dm));
    dm   = dma;
  }
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject) dm, "final_"));
  PetscCall(DMViewFromOptions(dm, NULL, "-dm_view"));
  
  // 4- write down the mesh
  if (dim == 2) {
    PetscCall(DMPlexWrite_gmfMesh2d(dm, PETSC_TRUE, bdLabelName, "out", 0, NULL,  NULL, NULL, NULL, PETSC_TRUE));
  }
  else if (dim == 3) {
    PetscCall(DMPlexWrite_gmfMesh3d(dm, PETSC_TRUE, bdLabelName, "out3d", 0, NULL,  NULL, NULL, NULL, PETSC_TRUE));
  }

  PetscCall(DMDestroy(&dm));
  PetscCall(PetscFinalize());

  return 0;
}


//-dm_plex_metric_h_min 0.001 -dm_plex_metric_h_max 1 -dm_plex_metric_a_max 1000 -dm_plex_metric_p 1.0 -dm_plex_metric_target_complexity 10000.0 -dm_adaptor mmg -adapt_dm_view -dm_plex_metric_verbosity 1