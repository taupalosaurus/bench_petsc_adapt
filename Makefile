# -*- mode: makefile -*-

#   You can set specific values below but that should rarely need to
CFLAGS		 = -I/home/barral/build/libMeshb/sources

run : main.o io_libmeshb.o
	$(LINK.C) -o $@ $^ $(LDLIBS) -L/home/barral/build/libMeshb/build/sources -lmeshb

# If the file c.cxx needs to link with a C++ standard library -lstdc++ , then
# you'll need to add it explicitly.  It can go in the rule above or be added to
# a target-specific variable by uncommenting the line below.
# app : LDLIBS += -lstdc++

include ${PETSC_DIR}/lib/petsc/conf/variables

#  To access the PETSc variables for the build, including compilers, compiler flags, libraries etc but
#  manage the build rules yourself (rarely needed) comment out the next lines
#
include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/test

