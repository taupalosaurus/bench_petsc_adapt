#include <libmeshb7.h>


PetscErrorCode DMPlexWrite_gmfMesh2d(DM dm, PetscBool writeMesh, const char bdLabelName[], const char meshName[], 
                                     PetscInt solNumber, Vec * sol,  PetscInt * solTypes, const char solName[],
                                     PetscSection section, PetscBool ascii);
PetscErrorCode DMPlexCreateGmfFromFile_2d(const char meshName[], const char bdLabelName[], DM *dm);
PetscErrorCode DMPlexReadGmfSolFromFile_2d(DM dm, PetscSection section, const char solName[], PetscInt solType, Vec * sol);
PetscErrorCode DMPlexReadGmfSolFromFile_2d_V2(DM dm, PetscSection section, const char solName[], PetscInt solNumber, const PetscInt solTypes[], Vec * sol);
PetscErrorCode DMPlexWrite_gmfMesh3d(DM dm, PetscBool writeMesh, const char bdLabelName[], const char meshName[], 
                                     PetscInt numSol, Vec * sol,  PetscInt * solTypes, const char * solNames[],
                                     PetscSection section, PetscBool ascii);
PetscErrorCode DMPlexCreateGmfFromFile_3d(const char meshName[], const char bdLabelName[], DM *dm);
PetscErrorCode DMPlexReadGmfSolFromFile_3d(DM dm, PetscSection section, const char solName[], PetscInt solType, Vec * sol);
